import {FlatList, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import ListKegiatan from '../../molecules/list-kegiatan';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import { COLORS } from '../../../constans';

const FlatlistKegiatan = (props: any) => {
  const {data} = props;

  const renderRightAction = () => {
    return (
      <View style={styles.viewRightAction}>
        <Text style={styles.textDelete}>Delete</Text>
      </View>
    );
  };
  const renderItem = (item: any) => {
    return (
      <Swipeable renderRightActions={renderRightAction} onSwipeableRightOpen={() => props.onDeleteList(item.item.id)}>
        <ListKegiatan nama={item.item.nama} jam={item.item.jam} status={item.item.status} onPressCheck={() => props.onPressCheck(item.item)} onPressList={() => props.onPressList(item.item)}/>
      </Swipeable>
    );
  };
  return (
    <View style={styles.viewContainer}>
      <FlatList data={data} renderItem={renderItem} />
    </View>
  );
};

export default FlatlistKegiatan;

const styles = StyleSheet.create({
    viewRightAction: {
        flexDirection: 'row-reverse',
        backgroundColor : COLORS.DEFAULT_COLOR,
        padding: 10,
        flex: 1,
    },
    textDelete: {
        color: '#ffffff',
    },
    viewContainer: {
        flex: 1
    }
});
