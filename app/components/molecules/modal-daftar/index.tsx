import { Modal, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import { COLORS } from '../../../constans'

const ModalDaftar = (props: any) => {
    const {onPressClose, onChangeTextUsername, onChangeTextEmail, onChangeTextPass, onPressDaftar} = props
    const [showUsername, setShowUsername] = useState(false)
    const [showEmail, setShowEmail] = useState(false)
    const [showPass, setShowPass] = useState(false)

  return (
    <Modal transparent={true} animationType='slide' onRequestClose={onPressClose}>
        <View style={styles.viewContainer}>
            <TouchableOpacity style={styles.buttonClose} onPress={onPressClose}/>
            <View style={styles.viewBody}>
                <View>
                    <Text style={styles.textJudul}>Silahkan Buat Akun Anda</Text>
                    {showUsername && <Text>Username</Text>}
                    <TextInput placeholder={showUsername ? '' : 'Username'}style={styles.textInput} onPressIn={() => setShowUsername(true)} onChangeText={onChangeTextUsername}/>
                    {showEmail && <Text>Email</Text>}
                    <TextInput placeholder={showEmail ? '' : 'Email'} style={styles.textInput} onPressIn={() => setShowEmail(true)} onChangeText={onChangeTextEmail}/>
                    {showPass && <Text>Password</Text>}
                    <TextInput placeholder={showPass ? '' : 'Password'} style={styles.textInput} onPressIn={() => setShowPass(true)} onChangeText={onChangeTextPass}/>
                    <TouchableOpacity style={styles.buttonDaftar} onPress={onPressDaftar}>
                        <Text style={styles.textDaftar}>Daftar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    </Modal>
  )
}

export default ModalDaftar

const styles = StyleSheet.create({
    viewContainer: {
        flex: 1,
        backgroundColor: COLORS.TRANSPARENT_GREY
    },
    viewBody: {
        backgroundColor: '#ffffff',
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        justifyContent: 'center',
        alignItems:'center'
    },
    textInput: {
        paddingVertical: 0,
        marginVertical: 5,
        borderWidth: 1,
        borderColor: 'lightgrey'
    },
    textJudul: {
        fontSize: 20,
        color: 'black',
        marginBottom: 20
    },
    buttonDaftar: {
        backgroundColor: COLORS.DEFAULT_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        paddingVertical: 10,
        borderRadius: 10
    },
    textDaftar: {
        color: '#ffffff'
    },
    buttonClose: {
        height: 50,
    }
})