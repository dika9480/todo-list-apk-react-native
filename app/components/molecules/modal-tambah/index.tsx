import { Modal, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import { COLORS } from '../../../constans'

const ModalTambahKegiatan = (props: any) => {
    const {onPressCloseModal, onCangeTextKegiatan, onCangeTextJam, onCangeTextMenit, onPressTambahKegiatan, type, kegiatan, jam, menit, onPressEditKegiatan} = props
  return (
    <Modal transparent={true} onRequestClose={onPressCloseModal}>
        <View style={styles.viewContainer}>
            <TouchableOpacity style={{flex:1}} onPress={onPressCloseModal}/>
            <View style={styles.viewBody}>
                <Text style={styles.textTambah}>{type === 'tambah' ? 'Tambah' : 'Edit'} Kegiatan</Text>
                <Text>Kegiatan</Text>
                <TextInput placeholder='Kegiatan' style={styles.textInput} onChangeText={onCangeTextKegiatan} value={kegiatan}/>
                <Text>Jam</Text>
                <View style={styles.viewJam}>
                <TextInput placeholder='00' style={styles.textInput} onChangeText={onCangeTextJam} value={jam} maxLength={2} keyboardType='numeric'/>
                <Text>:</Text>
                <TextInput placeholder='00' style={styles.textInput} onChangeText={onCangeTextMenit} value={menit} maxLength={2} keyboardType='numeric'/>
                </View>
                <View style={styles.viewbutton}>
                <TouchableOpacity style={styles.buttonTambah} onPress={type === 'tambah' ? onPressTambahKegiatan : onPressEditKegiatan}>
                    <Text style={styles.textButtonTambah}>{type === 'tambah' ? 'Tambah' : 'Edit'}</Text>
                </TouchableOpacity>
                </View>
            </View>
            <TouchableOpacity style={{flex:1}} onPress={onPressCloseModal}/>
        </View>
    </Modal>
  )
}

export default ModalTambahKegiatan

const styles = StyleSheet.create({
    viewContainer: {
        backgroundColor: COLORS.TRANSPARENT_GREY,
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    viewBody: {
        backgroundColor: '#ffffff',
        padding: 20,
        marginHorizontal: 20
    },
    textTambah: {
        color: 'black',
        fontWeight: '600',
        fontSize: 16,
        marginBottom: 20
    },
    textInput: {
        paddingVertical: 5,
        borderWidth:1,
        borderColor: 'lightgrey',
        marginVertical: 5,
    },
    viewJam:{
        flexDirection: 'row',
        width: '25%',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    viewbutton: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonTambah: {
        backgroundColor: COLORS.DEFAULT_COLOR,
        paddingHorizontal: 20,
        paddingVertical: 5,
        borderRadius: 5,
        marginTop: 10
    },
    textButtonTambah: {
        color: '#ffffff'
    }
})