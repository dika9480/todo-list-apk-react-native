import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { COLORS } from '../../../constans'

const ListKegiatan = (props: any) => {
    const {nama, jam, status, onPressCheck, onPressList} = props
  return (
    <TouchableOpacity style={styles.viewContainer} onPress={onPressList}>
      <Text style={styles.textNama}>{nama} - {jam}</Text>
      <TouchableOpacity style={styles.buttonStatus} onPress={onPressCheck}>
       {status && <View style={styles.viewCheck}/>}
      </TouchableOpacity>
    </TouchableOpacity>
  )
}

export default ListKegiatan

const styles = StyleSheet.create({
    viewContainer: {
        flexDirection: 'row',
        backgroundColor :'#ffffff',
        padding: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#eeeeee',
        justifyContent: 'space-between',
    },
    textNama: {
        color: 'black'
    },
    buttonStatus: {
        borderWidth: 1,
        borderColor: 'lightgrey',
        width: 20,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    viewCheck: {
        backgroundColor :COLORS.DEFAULT_COLOR,
        width: 13,
        height: 13,
        borderRadius: 10
    }
})