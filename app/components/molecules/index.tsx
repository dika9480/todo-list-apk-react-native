import Loading from "./Loading";
import ListKegiatan from "./list-kegiatan";
import ModalTambahKegiatan from "./modal-tambah";
import ModalDaftar from "./modal-daftar";
import ModalLogout from "./modal-logout";

export  {Loading, ListKegiatan, ModalTambahKegiatan, ModalDaftar, ModalLogout}