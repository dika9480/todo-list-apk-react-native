import { Modal, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { COLORS } from '../../../constans'

const ModalLogout = (props: any) => {
    const {onPressClose, onPressYa} = props
  return (
    <Modal transparent={true} onRequestClose={onPressClose}>
        <View style={styles.viewContainer}>
            <TouchableOpacity style={styles.buttonClose} onPress={onPressClose}/>
            <View style={styles.viewBody}>
                <Text style={styles.textWarn}>Apakah anda yakin mau logout?</Text>
                <View style={styles.viewButton}>
                    <TouchableOpacity style={styles.button} onPress={onPressClose}>
                        <Text style={styles.textButton}>Tidak</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={onPressYa}>
                        <Text style={styles.textButton}>Ya</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <TouchableOpacity style={styles.buttonClose} onPress={onPressClose}/>
        </View>
    </Modal>
  )
}

export default ModalLogout

const styles = StyleSheet.create({
    viewContainer: {
        flex: 1,
        backgroundColor : COLORS.TRANSPARENT_GREY
    },
    buttonClose: {
        flex: 1,
    },
    viewBody: {
        backgroundColor :'#ffffff',
        marginHorizontal: 40,
        padding: 20,
        justifyContent: 'center',
        alignItems:'center'
    },
    viewButton: {
        flexDirection :'row',
        justifyContent: 'space-between'
    },
    button: {
        backgroundColor :COLORS.DEFAULT_COLOR,
        paddingHorizontal: 50,
        paddingVertical: 5,
        borderRadius: 5,
        marginTop: 20,
        marginHorizontal: 5
    },
    textButton: {
        color: '#ffffff',
        fontWeight: '500',
    },
    textWarn: {
        fontWeight: '500',
        color: 'black',
        fontSize: 15
    }
})