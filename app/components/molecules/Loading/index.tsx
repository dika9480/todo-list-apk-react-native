import {ActivityIndicator, Modal, StyleSheet, Text, View} from 'react-native';
import React from 'react';

const Loading = () => {
  return (
    <Modal transparent={true}>
      <View style={styles.viewContainer}>
        <View style={styles.viewBody}>
          <ActivityIndicator size={50} color={'#43a047'}/>
            <Text>Silahkan Tunggu</Text>
        </View>
      </View>
    </Modal>
  );
};

export default Loading;

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0, 0.5)',
    justifyContent: 'center',
    alignItems:'center'
  },
  viewBody: {
    backgroundColor: '#ffffff',
    padding: 10,
  },
});
