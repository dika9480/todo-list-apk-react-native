import {
  StatusBar,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
  Image
} from 'react-native';
import React, {useEffect, useState} from 'react';
import database from '@react-native-firebase/database';
import FlatlistKegiatan from '../../components/organism/flatlist-kegiatan';
import {COLORS} from '../../constans';
import {Loading, ModalLogout, ModalTambahKegiatan} from '../../components';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { IMG } from '../../constans/images';

const HomeScreen = (props: any) => {
  const {navigation, route} = props
  const {username} = route.params
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [showModalTambah, setShowModalTambah] = useState(false);
  const [kegiatan, setKegiatan] = useState('');
  const [jam, setJam] = useState('');
  const [menit, setMenit] = useState('');
  const [showModalEdit, setShowModalEdit] = useState(false)
  const [dataKegiatan, setDataKegiatan] = useState({})
  const [showDataKosong, setShowDataKosong] = useState(false)
  const [showModalLogout, setShowModalLogout] = useState(false)

  console.log(username)

  //function get data
  const getData = async () => {
    let token = await AsyncStorage.getItem('token')
    setIsLoading(true);
    setData([]);
    database()
      .ref(`User/${token}/kegiatan`)
      .once('value')
      .then(item => {
        console.log('item', item)
        if(item.val() === null){
          setShowDataKosong(true)
          console.log('kosong')
          setIsLoading(false)
        } else {
          setShowDataKosong(false)
          item.forEach((items: any) => {
            console.log(items);
            setData(data => [...data, items.val()]);
            setIsLoading(false);
            console.log(data);
          });
        }
      });
  };
  
  useEffect(() => {
    getData();
  }, []);

  //function insert data
  const onPressTambahKegiatan = async () => {
    console.log(kegiatan)
    console.log(jam)
    console.log(menit)
    let token = await AsyncStorage.getItem('token')
    if(kegiatan.length === 0){
      ToastAndroid.show('Kegiatan masih kosong', ToastAndroid.SHORT)
    } else if(jam.length === 0){
      ToastAndroid.show('Jam masih kosong', ToastAndroid.SHORT)
    } else if(menit.length === 0){
      ToastAndroid.show('Menit masih kosong', ToastAndroid.SHORT)
    } else {
      const newReference = database().ref(`User/${token}/kegiatan`).push();
      console.log('Auto generated key: ', newReference.key);
      newReference
      .set({
        id: newReference.key,
        nama: kegiatan,
        jam: `${jam}:${menit}`,
        status: false,
      })
      .then(() => {getData(), ToastAndroid.show('Berhasil menambahkan data', ToastAndroid.SHORT), setShowModalTambah(false);});
    }
  };


  //function delete data
  const onDeleteList = async (item: any) => {
    let token = await AsyncStorage.getItem('token')
    await database()
      .ref(`User/${token}/kegiatan/${item}`)
      .remove()
      .then(() =>
        ToastAndroid.show('Berhasil menghapus data', ToastAndroid.SHORT),
      );
    getData();
  };

  //funtion update data
  const updateDataStatus = async (item: any) => {
    let token = await AsyncStorage.getItem('token')
      await database()
      .ref(`User/${token}/kegiatan/${item.id}`)
      .update({
        id: item.id,
        nama: item.nama,
        jam: item.jam,
        status: item.status ? false : true,
      })
      .then(() => {getData(), ToastAndroid.show('Berhasil update data', ToastAndroid.SHORT)});
  }

  const onCangeTextKegiatan = (item: any) => {
    setKegiatan(item);
  };
  const onCangeTextJam = (item: any) => {
    setJam(item);
  };
  const onCangeTextMenit = (item: any) => {
    setMenit(item);
  };

  const onPressList = (item: any) => {
    setJam(item.jam[0] + item.jam[1])
    setMenit(item.jam[3] + item.jam[4])
    setKegiatan(item.nama)
    setShowModalEdit(true)
    setDataKegiatan(item)
  }
  const onPressEditKegiatan = async () => {
    let token = await AsyncStorage.getItem('token')
    await database()
      .ref(`User/${token}/kegiatan/${dataKegiatan.id}`)
      .update({
        id: dataKegiatan.id,
        nama: kegiatan,
        jam: `${jam}:${menit}`,
        status: dataKegiatan.status
      })
      .then(() => {getData(), ToastAndroid.show('Berhasil update data', ToastAndroid.SHORT), setShowModalEdit(false)});
  }
  const onPressPlus = () => {
    setKegiatan('')
    setJam('')
    setMenit('')
    setShowModalTambah(true)
  }
  const onPressShowModalLogout = () => {
    setShowModalLogout(true)
  }
  const onPressLogout = async () => {
    navigation.replace('NAVIGATION_LOGIN')
    await AsyncStorage.setItem('token', '')
    await AsyncStorage.setItem('username', '')
    await AsyncStorage.setItem('login', '')
  }
  return (
    <View style={styles.viewContainer}>
      <StatusBar backgroundColor={COLORS.DEFAULT_COLOR} />
      <View style={styles.viewHeader}>
        <View style={styles.viewTextHeader}>
        <Text style={styles.textHeader}>TO DO LIST</Text>
        </View>
        <TouchableOpacity onPress={onPressShowModalLogout}>
        <Image source={IMG.ICON_LOGOUT}/>
        </TouchableOpacity>
      </View>
      <View style={styles.viewNama}>
        <Text style={styles.textNama}>Hi {username}!</Text>
      </View>
      {showDataKosong  ? 
      <View style={styles.viewDataKosong}>
        <Text style={styles.textDataKosong}>Data kegiatan masih kosong, silahkan tambahkan terlebih dahulu!!</Text>
      </View> : <FlatlistKegiatan data={data} onDeleteList={onDeleteList} onPressCheck={updateDataStatus} onPressList={onPressList}/>}
      <View style={styles.viewButtonTambah}>
        <TouchableOpacity
          style={styles.buttonTambah}
          onPress={onPressPlus}>
          <Text style={styles.textPlus}>+</Text>
        </TouchableOpacity>
      </View>
      {isLoading && <Loading />}
      {showModalTambah && (
        <ModalTambahKegiatan
          onPressCloseModal={() => setShowModalTambah(false)}
          onCangeTextKegiatan={onCangeTextKegiatan}
          onCangeTextJam={onCangeTextJam}
          onCangeTextMenit={onCangeTextMenit}
          onPressTambahKegiatan={onPressTambahKegiatan}
          type={'tambah'}
        />
      )}
      {showModalEdit && (
        <ModalTambahKegiatan
          onPressCloseModal={() => setShowModalEdit(false)}
          onCangeTextKegiatan={onCangeTextKegiatan}
          onCangeTextJam={onCangeTextJam}
          onCangeTextMenit={onCangeTextMenit}
          type={'edit'}
          kegiatan={kegiatan}
          jam={jam}
          menit={menit}
          onPressEditKegiatan={onPressEditKegiatan}
        />
      )}
      {showModalLogout && <ModalLogout onPressClose={() => setShowModalLogout(false)} onPressYa={onPressLogout}/>}
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  buttonTambah: {
    backgroundColor: COLORS.DEFAULT_COLOR,
    paddingHorizontal: 20,
    height: 50,
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
  },
  viewHeader: {
    backgroundColor: COLORS.DEFAULT_COLOR,
    paddingVertical: 15,
    borderTopWidth: 1,
    borderTopColor: 'lightgrey',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20
  },
  textHeader: {
    color: '#ffffff',
    textAlign: 'center',
    fontWeight: '500',
    fontSize: 16,
  },
  viewButtonTambah: {
    flexDirection: 'row-reverse',
    marginBottom: 30,
    marginHorizontal: 30,
  },
  textPlus: {
    fontSize: 20,
    color: '#ffffff',
  },
  viewDataKosong: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textDataKosong: {
    textAlign: 'center'
  },
  viewNama: {
    justifyContent: 'center',
    alignItems:'center',
    paddingVertical: 5,
  },
  textNama: {
    fontSize: 16,
    color: 'black',
    fontWeight: '500',
    textDecorationLine: 'underline'
  },
  viewTextHeader: {
    flex: 1
  }
});
