import {
  StyleSheet,
  Text,
  TextInput,
  ToastAndroid,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import database, {firebase} from '@react-native-firebase/database';
import { Loading, ModalDaftar } from '../../components';
import AsyncStorage from '@react-native-async-storage/async-storage'

const LoginScreen = (props: any) => {
  const {navigation} = props;
  const [showUsername, setShowUsername] = useState(true);
  const [showPassword, setShowPassword] = useState(false);
  const refUsername = useRef();
  const refPassword = useRef();
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  const [showModalDaftar, setShowModalDaftar] = useState(false)
  const [usernameDaftar, setUsernameDaftar]= useState('')
  const [emailDaftar, setEmailDaftar] = useState('')
  const [passDaftar, setPassDaftar] = useState('')
  const [data, setData] = useState([])


  const getData = async () => {
    let login = await AsyncStorage.getItem('login')
    let token = await AsyncStorage.getItem('token')
    let username = await AsyncStorage.getItem('username')
    console.log({token})
    console.log({login})
    console.log({username})
    if(token !== null){
      navigation.replace('NAVIGATION_HOME', {username : username})
    } else {
      setData([])
    //funtion get data dari realtime database firebase
    database()
    .ref('/User')
    .once('value')
    .then(snapshot => {
      console.log(snapshot.val())
      snapshot.forEach((item: any) => {
        setData(data => [...data, item.val()]);
      })
    }
  )
    }
    
}


  useEffect(() => {
    refUsername.current.focus();
    getData()
  }, []);
  
  const onSubmitUsername = () => {
    if(username.length === 0){
      ToastAndroid.show('Masukan Usernamae', ToastAndroid.SHORT)
    } else {
      refPassword.current.focus()
      setShowPassword(true)
    }
  }
  
  const onSubmitPassword = () => {
    if(password.length === 0){
      ToastAndroid.show('Masukan Password', ToastAndroid.SHORT)
    } else {
      onPressLogin()
    }
  }

  const onPressLogin = async () => {
    setIsLoading(true)
    let dataFilter = data.filter(x => x.username === username && x.password === password)
    console.log(dataFilter)
    if(dataFilter.length === 0){
      ToastAndroid.show('Periksa lagi username dan password anda', ToastAndroid.SHORT)
    } else {
      navigation.replace('NAVIGATION_HOME', {username : dataFilter[0].username})
      await AsyncStorage.setItem('token', dataFilter[0].id)
      await AsyncStorage.setItem('username', dataFilter[0].username)
      await AsyncStorage.setItem('login', 'true')
    }
    setIsLoading(false)
  }

  const onPressDaftar = () => {
    setShowModalDaftar(true)
  }
  const onChangeTextUsername = (item: string) => {
    setUsernameDaftar(item)
  }
  const onChangeTextEmail = (item: string) => {
    setEmailDaftar(item)
  }
  const onChangeTextPass = (item: any) => {
    setPassDaftar(item)
  }
  const onPressDaftarModal = () => {
    if(usernameDaftar.length === 0){
      ToastAndroid.show('Username masih kosong', ToastAndroid.SHORT)
    } else if(emailDaftar.length === 0){
      ToastAndroid.show('Email masih kosong', ToastAndroid.SHORT)
    } else if(passDaftar.length === 0){
      ToastAndroid.show('Password masih kosong', ToastAndroid.SHORT)
    } else {
      const newReference = database().ref('/User').push();
      newReference
      .set({
        id: newReference.key,
        username: usernameDaftar,
        email: emailDaftar,
        password: passDaftar,
      })
      .then(() => {getData(), ToastAndroid.show('Berhasil membuat akun', ToastAndroid.SHORT), setShowModalDaftar(false);});
    }
  }
  return (
    <View style={styles.viewContainer}>
      <View style={styles.viewBody}>
        <Text style={styles.textSelamatDatang}>Selamat Datang</Text>
        {showUsername && <Text>Username</Text>}
        <TextInput
          placeholder={showUsername ? '' : 'Username'}
          style={styles.textInput}
          onPressIn={() => setShowUsername(true)}
          ref={refUsername}
          onSubmitEditing={onSubmitUsername}
          onChangeText={(item: string) => setUsername(item)}
        />
        {showPassword && <Text>Password</Text>}
        <TextInput
          placeholder={showPassword ? '' : 'Password'}
          style={styles.textInput}
          onPressIn={() => setShowPassword(true)}
          ref={refPassword}
          onSubmitEditing={onSubmitPassword}
          onChangeText={(item: string) => setPassword(item)}
        />
      </View>
      <TouchableOpacity
        onPress={onPressLogin}
        style={styles.buttonLogin}>
        <Text style={styles.textLogin}>Login</Text>
      </TouchableOpacity>
      <Text style={styles.textSignin}>Belum punya akun?
        <Text style={styles.textDaftar} onPress={onPressDaftar}> Daftar</Text>
      </Text>
      {isLoading && <Loading/>}
      {showModalDaftar && <ModalDaftar 
      onPressClose={() => setShowModalDaftar(false)} 
      onChangeTextUsername={onChangeTextUsername}
      onChangeTextEmail={onChangeTextEmail}
      onChangeTextPass={onChangeTextPass}
      onPressDaftar={onPressDaftarModal}
      />}
    </View>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLogin: {
    backgroundColor: '#43a047',
    paddingHorizontal: 40,
    paddingVertical: 10,
    borderRadius: 10,
  },
  textLogin: {
    color: '#ffffff',
  },
  textInput: {
    borderWidth: 1,
    borderColor: 'lightgrey',
    paddingVertical: 0,
    marginVertical: 10,
    paddingLeft: 5,
  },
  viewBody: {
    width: '70%',
  },
  textSelamatDatang: {
    textAlign: 'center',
    color: 'black',
    fontWeight: '500',
    fontSize: 18,
  },
  textSignin: {
    marginTop: 20,
    fontSize: 15,
    color: 'black'
  },
  textDaftar: {
    color: 'blue'
  }
});
